import configparser
import os


def write_config(category, key, value):
    if os.environ.get("XDG_CONFIG_HOME") is None:
        configfilepath = f"{os.environ.get('HOME')}/.config/contentdb-cli/cli.ini"
    else:
        configfilepath = f"{os.environ.get('XDG_CONFIG_HOME')}/contentdb-cli/cli.ini"
    config = configparser.ConfigParser()
    config.read(configfilepath)
    try:
        config.add_section(category)
    except configparser.DuplicateSectionError:
        pass
    config.set(category, key, value)
    if os.environ.get("XDG_CONFIG_HOME") is None:
        configfilepath = f"{os.environ.get('HOME')}/.config/contentdb-cli/cli.ini"
    else:
        configfilepath = f"{os.environ.get('XDG_CONFIG_HOME')}/contentdb-cli/cli.ini"
    os.makedirs(os.path.dirname(configfilepath), exist_ok=True)
    with open(configfilepath, 'w') as configfile:
        config.write(configfile)


def read_config(category, key):
    if os.environ.get("XDG_CONFIG_HOME") is None:
        configfilepath = f"{os.environ.get('HOME')}/.config/contentdb-cli/cli.ini"
    else:
        configfilepath = f"{os.environ.get('XDG_CONFIG_HOME')}/contentdb-cli/cli.ini"
    try:
        os.makedirs(os.path.dirname(configfilepath), exist_ok=True)
        config = configparser.ConfigParser()
        config.read(configfilepath)
        return config.get(category, key)
    except:
        return None
