import configparser
import glob
import os
import shutil
import zipfile
from io import BytesIO

import click
import requests
from _config import read_config, write_config
from _helpers import write_dot_conf_file, is_upgradable
from git import Repo, Git, GitCommandError
from pycontentdb import ContentDB
from termcolor import cprint

content_db = ContentDB()
config = configparser.ConfigParser()

content_folder_map = {
    "mod": "mods",
    "game": "games",
    "txp": "textures"
}


@click.group()
@click.pass_context
def main(ctx):
    if read_config("default", "content_directory") is None:
        setup([])


@main.command()
@click.pass_context
def setup(ctx):
    minetest_dir = input(f"Please enter your minetest directory ({os.environ.get('HOME')}/.minetest) ")
    if minetest_dir.strip() == "":
        minetest_dir = f"{os.environ.get('HOME')}/.minetest"
    if minetest_dir[-1] != '/':
        minetest_dir = f"{minetest_dir}/"
    write_config("default", "content_directory", minetest_dir)


@main.command()
@click.argument('package')
@click.option('--amount')
@click.pass_context
def search(ctx, package, amount: int):
    if amount is None:
        amount = 5
    amount = int(amount)
    results = content_db.search(package)[:amount]
    for idx, result in enumerate(results):
        cprint(f"{idx}. {result['name']}", "red", attrs=["bold"])
        cprint(f"Author: {result['author']}", "white")
        cprint(f"Type: {result['type']}", "white")
        cprint(result["short_description"], "white")
        cprint("-----------------------", "yellow")


@main.command()
@click.argument('package_name')
@click.option('--index')
@click.option('--author')
@click.pass_context
def install(ctx, package_name, index: int = 0, author=None):
    if index is None:
        index = 0
    index = int(index)

    if author:
        package = content_db.get(author, package_name)
    else:
        package = content_db.search(package_name)[index]
    package_details = content_db.get(package["author"], package["name"])
    release = content_db.get_release(package["author"], package["name"])[0]
    cprint(f"Installing {package_details['name']} from {package_details['author']}")
    continue_input = input("Do you want to continue? (Y/n) ")
    if continue_input == "Y" or continue_input == "":
        if release["commit"]:
            cprint("Cloning git repository...", "yellow")
            try:
                Repo.clone_from(package_details["repo"],
                                f"{read_config('default', 'content_directory')}"
                                f"{content_folder_map[package_details['type']]}/{package_details['name']}")
            except GitCommandError:
                cprint("Already Installed, updating...", color="yellow")
            cprint("Checking out git repository to latest release...", "yellow")
            repo = Git(
                f"{read_config('default', 'content_directory')}"
                f"{content_folder_map[package_details['type']]}/{package_details['name']}")
            repo.checkout(release["commit"])
            write_dot_conf_file(package_details)
        else:
            cprint("Unable to find commit for release, using direct download...", "yellow")
            url = release["url"]
            if url.startswith("/uploads"):
                url = content_db.base_url + url
            zip_data = requests.get(url)
            file = zipfile.ZipFile(BytesIO(zip_data.content))
            path = f'{read_config("default", "content_directory")}{content_folder_map[package_details["type"]]}'
            if file.namelist()[1] == "":
                path = f"{path}/{package_details['name']}"
            file.extractall(path=f"{path}")
            try:
                os.rename(
                    f"{path}{content_folder_map[package_details['type']]}/{package_details['name']}"
                    f"-{release['url'].split('/')[-1].split('.')[-2]}",
                    f"{path}{content_folder_map[package_details['type']]}/{package_details['name']}")
            except FileNotFoundError:
                pass
    else:
        cprint("Aborting...", "red")


@main.command()
@click.argument('content_type')
@click.argument('package_name')
@click.pass_context
def uninstall(ctx, content_type, package_name):
    if content_type not in ["mod", "game", "txp"]:
        cprint("Invalid type. Valid types are: mod, game and txp.", "red")
    cprint(f"Uninstalling {package_name}", "yellow")
    continue_input = input("Do you want to continue? (Y/n) ")
    if continue_input == "Y" or continue_input == "":
        try:
            shutil.rmtree(f'{read_config("default", "content_directory")}{content_folder_map[content_type]}'
                          f'/{package_name}')
        except FileNotFoundError:
            cprint("No such package found.", "red")


@main.command()
@click.pass_context
def upgrade(ctx):
    cprint("Searching for available Upgrades", "yellow")
    all_content = []
    content_types = ["mods", "games", "textures"]
    for content_type in content_types:
        for folder in glob.glob(f'{read_config("default", "content_directory")}{content_type}/*', recursive=True):
            all_content.append((folder, content_type))
    for content, content_type in all_content:
        is_content_upgradable, author = (is_upgradable(content, content_type))
        if is_content_upgradable:
            cprint("Out of date, updating now...", "yellow")
            ctx.invoke(install, package_name=content.split("/")[-1], author=author)
        else:
            cprint("Up to date", "green")
        cprint("-------------", "white")


def start():
    main(obj={})


if __name__ == '__main__':
    start()
