from _config import read_config
from pycontentdb import ContentDB
from termcolor import cprint

content_db = ContentDB()

content_folder_map = {
    "mod": "mods",
    "game": "games",
    "txp": "textures"
}


def get_config_from_file(path):
    try:
        with open(path, "r") as file:
            lines = file.readlines()
            config = {}
            for line in lines:
                key, value = line.split("=")
                key = key.strip()
                value = value.strip()
                config[key] = value
            return config
    except FileNotFoundError:
        return {}


def write_config_to_file(path, data):
    file_data = ""
    for key, value in data.items():
        file_data = file_data + f"{key} = {value}\n"

    with open(path, "w") as file:
        file.write(file_data)


def write_dot_conf_file(package: dict):
    conf_name = ""
    if package["type"] == "mod":
        if len(package["provides"]) > 1:
            conf_name = "modpack.conf"
        else:
            conf_name = "mod.conf"
    elif package["type"] == "game":
        conf_name = "game.conf"
    elif package["type"] == "txp":
        conf_name =\
            "texture_pack.conf"
    conf_path = f"{read_config('default', 'content_directory')}" \
                f"{content_folder_map[package['type']]}/{package['name']}/{conf_name}"
    config = get_config_from_file(conf_path)
    config["name"] = package["name"]
    config["author"] = package["author"]
    config["description"] = package["short_description"]
    config["release"] = package["release"]
    config["title"] = package["title"]
    write_config_to_file(conf_path, config)


folder_config_map = {
    "mods": "mod.conf",
    "games": "game.conf",
    "textures": "texture_pack.conf"
}


def is_upgradable(package_name, folder):
    cprint(f"Checking {package_name.split('/')[-1]}", "green")
    subfolder = package_name.split("/")[-2]
    current_config = get_config_from_file(f"{package_name}/{folder_config_map[subfolder]}")
    if current_config == {} and subfolder == "mods":
        current_config = get_config_from_file(f"{package_name}/modpack.conf")
    try:
        current_release = current_config["release"]
    except KeyError:
        cprint("Unable to find current release, skipping...", "yellow")
        return False, None
    author = current_config["author"]
    content_db_package = content_db.get(author, package_name.split("/")[-1])
    newest_release = content_db_package["release"]
    # print(newest_release, current_release)
    return int(newest_release) > int(current_release), author
