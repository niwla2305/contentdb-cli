# contentdb-cli
This projects goal is to make it easy to install and manage minetest content like mods,
games or texturepacks.

## Installation
### Debian / Ubuntu /Mint
You can find .deb builds in CI/CD under latest pipelines artifacts.
### Manual
* Clone this repository
* `pip3 install -r requirements.txt`
* Run `contentdb-cli/contentdb.py`

### Windows
Currently there is no Windows support for this project. It may or may not be added later.
When you got python running, you wont be able to use this, because of things like config dirs

## Contributing
Contributing to the Project is welcome. By making a PR you agree that your code is licensed under MIT.