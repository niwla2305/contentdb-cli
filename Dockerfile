FROM registry.gitlab.com/minetest/minetest/server

USER root
WORKDIR /usr/bin/contentdb-cli/

COPY requirements.txt /usr/bin/contentdb-cli/
RUN apk add --no-cache python3 py-pip git nano
RUN pip3 install --no-cache-dir -r requirements.txt


COPY contentdb-cli/* /usr/bin/contentdb-cli/
COPY contentdb /usr/bin/
RUN chmod +x /usr/bin/contentdb
USER minetest